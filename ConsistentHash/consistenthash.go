package ConsistentHash

import (
	"hash/crc32"
	"sort"
	"strconv"
)

// Hash maps bytes to uint32
type Hash func(data []byte) uint32

// Map constains all hashed keys
type Map struct {
	hash     Hash
	replicas int            //虚拟节点倍数
	hashLoop []int          // Sorted
	hashMap  map[int]string //虚拟节点与真实节点的映射表
}

// New creates a Map instance
func New(replicas int, fn Hash) *Map {
	m := &Map{
		replicas: replicas,
		hash:     fn,
		hashMap:  make(map[int]string),
	}
	if m.hash == nil {
		m.hash = crc32.ChecksumIEEE
	}
	return m
}

// 真实节点的加入
func (m *Map) Add(serverNames ...string) {
	for _, serverName := range serverNames {
		for i := 0; i < m.replicas; i++ { //对每个真实节点映射为replicas个虚拟节点
			hash := int(m.hash([]byte(strconv.Itoa(i) + serverName))) // Itoa is equivalent to FormatInt(int64(i), 10).
			m.hashLoop = append(m.hashLoop, hash)
			m.hashMap[hash] = serverName
		}
	}
	sort.Ints(m.hashLoop)
}

// Get gets the closest item in the hash to the provided key.
func (m *Map) Get(key string) string {
	if len(m.hashLoop) == 0 {
		return ""
	}

	hash := int(m.hash([]byte(key)))

	//index := sort.Search(n int,f func(i int) bool) int
	//该函数使用二分查找的方法，会从[0, n)中取出一个值index，index为[0, n)中最小的使函数f(index)为True的值，并且f(index+1)也为True。
	//如果无法找到该index值，则该方法为返回n。

	idx := sort.Search(len(m.hashLoop), func(i int) bool { //要求哈希环必须递增
		return m.hashLoop[i] >= hash
	})

	return m.hashMap[m.hashLoop[idx%len(m.hashLoop)]]
}
