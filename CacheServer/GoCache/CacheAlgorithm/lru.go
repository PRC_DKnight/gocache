package CacheAlgorithm

import (
	"errors"
)

type LRUCache struct {
	maxBytes   uint64
	nBytes     uint64
	doubleList *DoubleList
	cacheMap   map[string]*ListNode
}

type Entry struct {
	Key string
	val Value
}

func NewEntry(key string, val Value) *Entry {
	return &Entry{Key: key, val: val}
}
func NewLRUCache(maxBytes uint64) ICacheAlgorithm {
	var a ICacheAlgorithm = &LRUCache{
		maxBytes:   maxBytes,
		doubleList: NewDoubleList(),
		cacheMap:   make(map[string]*ListNode),
	}
	return a
}

//1.检查是否存在key
func (c *LRUCache) Put(key string, val Value) {
	if oldValListNode, ok := c.cacheMap[key]; ok { //插入过（更新）
		oldValListNode.Entry.val = val
		c.doubleList.MoveToLast(oldValListNode)
	} else { //未插入过
		dataLen := uint64(len(key) + val.Len())
		for c.nBytes+dataLen > c.maxBytes {
			c.RemoveOldest()
		}
		c.nBytes += dataLen

		NewNode := NewListNodeWithEntry(NewEntry(key, val))
		c.cacheMap[key] = NewNode
		c.doubleList.AddToLast(NewNode)

	}
}

func (c *LRUCache) Get(key string) (Value, bool) {
	if listNode, ok := c.cacheMap[key]; ok { //插入过
		c.doubleList.MoveToLast(listNode)
		return listNode.Entry.val, true
	} else { //未插入过
		return nil, false
	}
}
func (c *LRUCache) Remove(key string) error { //没有该元素怎么办
	if delListNode, ok := c.cacheMap[key]; ok { //插入过
		delKey, delLen := c.doubleList.DeleteNode(delListNode)
		delete(c.cacheMap, delKey)
		c.nBytes -= uint64(len(delKey) + delLen)
		return nil
	} else { //未插入过
		return errors.New("KEY[" + key + "]NOT FOUND!")
	}
}

func (c *LRUCache) RemoveOldest() {
	delKey, delLen := c.doubleList.DeleteOldest()
	delete(c.cacheMap, delKey)
	c.nBytes -= uint64(len(delKey) + delLen)
}
