package CacheAlgorithm

type ICacheAlgorithm interface { //淘汰算法抽象层 后期可实现多个淘汰算法
	Put(key string, val Value)
	Get(key string) (Value, bool)
	Remove(key string) error
	RemoveOldest() //淘汰
}

//包装string类型实现LEN()便可以作为缓存值value
type String string

func (d String) Len() int { //实现了LEN方法
	return len(d)
}

//缓存值value可以使所有实现了Len()方法的type
type Value interface {
	Len() int
}
