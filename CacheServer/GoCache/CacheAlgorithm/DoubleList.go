package CacheAlgorithm

type DoubleList struct {
	head *ListNode
	tail *ListNode
}
type IDoubleList interface {
	DeleteOldest() (string, int) //删除最久未使用
	MoveToLast(node *ListNode)   //移动到链表尾部  最近使用
	AddToLast(node *ListNode)
	DeleteNode(node *ListNode) (string, int)
}
type ListNode struct {
	next  *ListNode
	pre   *ListNode
	Entry *Entry
}

func NewListNode() *ListNode {
	return &ListNode{
		next:  nil,
		pre:   nil,
		Entry: nil,
	}
}
func NewListNodeWithEntry(entry *Entry) *ListNode {
	return &ListNode{
		next:  nil,
		pre:   nil,
		Entry: entry,
	}
}

func (d *DoubleList) DeleteOldest() (string, int) {
	DelNode := d.head.next
	d.head.next = d.head.next.next
	d.head.next.pre = d.head //取消对头部元素的引用
	return DelNode.Entry.Key, DelNode.Entry.val.Len()
}

func (d *DoubleList) DeleteNode(node *ListNode) (string, int) {
	node.pre.next = node.next
	node.next.pre = node.pre
	return node.Entry.Key, node.Entry.val.Len()
}
func (d *DoubleList) MoveToLast(node *ListNode) {
	node.pre.next = node.next
	node.next.pre = node.pre
	d.AddToLast(node)
}

func (d *DoubleList) AddToLast(node *ListNode) {
	d.tail.pre.next = node
	node.pre = d.tail.pre
	node.next = d.tail
	d.tail.pre = node
}

func NewDoubleList() *DoubleList {
	d := &DoubleList{
		head: NewListNode(),
		tail: NewListNode(),
	}
	d.head.next = d.tail
	d.tail.pre = d.head
	return d
}
