package GoCache

import (
	"GOCache/conf"
	"fmt"
	"log"
	"sync"
)

type Group struct { //一个group可类似于一个数据库
	GroupID   int
	mainCache Cache //实现的并发缓存
}

var ( //全局变量
	mu     sync.RWMutex
	groups = make(map[int]*Group)
	group1 = NewGroup(0) //实例化一个group全局可用
)

func NewGroup(id int) *Group {
	mu.Lock()
	defer mu.Unlock()
	g := &Group{
		GroupID: id,
		//getter:    getter,
		mainCache: Cache{cacheBytes: conf.Config.MaxBytes, algorithm: conf.Config.CacheAlgorithm},
	}
	groups[id] = g
	return g
}

//根据id返回实例指针
func GetGroup(groupID int) *Group {
	mu.RLock()
	g := groups[groupID]
	mu.RUnlock()
	return g
}

//包装get方法
func (g *Group) Get(key string) (ByteView, error) {
	if key == "" {
		return ByteView{}, fmt.Errorf("key is required")
	}

	if v, ok := g.mainCache.get(key); ok {
		log.Println("[GOCache] hit")
		return v, nil
	}
	return ByteView{}, fmt.Errorf("[" + key + "]key Not Found!")
}

//包装put方法
func (g *Group) Put(key string, value ByteView) {
	g.mainCache.put(key, value)
}
