package GoCache

//抽象了一个只读数据结构 ByteView 用来表示缓存值
type ByteView struct {
	Bytes []byte //存储真实字节 可存图片等
}

// 实现了Len方法，ByteView是value的实现类，可以作为缓存的val
func (v ByteView) Len() int {
	return len(v.Bytes)
}

// ByteSlice 返回一个比特切片的拷贝
func (v ByteView) ByteSlice() []byte {
	return cloneBytes(v.Bytes)
}

// byte[]转为string的方法
func (v ByteView) String() string {
	return string(v.Bytes)
}

func cloneBytes(b []byte) []byte {
	c := make([]byte, len(b))
	copy(c, b)
	return c
}
