package CacheServer

import (
	"GOCache/CacheServer/GoCache"
	"GOCache/Common"
	"errors"
	"log"
)

func Handler(c *Connection, cmd string, key string, val string) (res []byte, err error) {
	switch cmd {
	case Common.CmdSet: //set命令
		val := GoCache.ByteView{Bytes: []byte(val)}
		g := GoCache.GetGroup(0)
		g.Put(key, val)
		log.Println("{", key, ":", val, "}", "插入成功")
		c.SendMsg([]byte(Common.RES_OK))
		return []byte(Common.RES_OK), nil
	case Common.CmdGet: //todo 现在只有一个group
		g := GoCache.GetGroup(0)
		val, err := g.Get(key)
		if err != nil {
			c.SendMsg([]byte(Common.RES_ERR_NOT_FOUND))
			return nil, nil
		} else {
			c.SendMsg([]byte("+" + string(val.Bytes) + "\r\n"))
			return nil, nil
		}
	default:
		c.SendMsg([]byte(Common.RES_ERR_Invalid_Synatx))
		return nil, errors.New("Command error:don't have such command:" + cmd)
	}

}
