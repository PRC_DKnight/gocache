package CacheServer

import (
	"GOCache/Common"
	"GOCache/conf"
	"bufio"
	"fmt"
	"log"
	"math"
	"net"
	"strconv"
	"time"
)

type IServer interface {
	Start()
	Stop()
	Serve()
}
type Server struct {
	//ServerID   uint32
	ServerName string
	IPVersion  string
	IP         string
	Port       int
	PassWord   string //加入校验密码，在连接建立时交换，若不正确则关闭连接
	ProxyIP    string
	DeployMode string
}

func NewServer() *Server {
	curProxyIP := ""
	if conf.Config.DeployMode == "ProxyMode" {
		curProxyIP = conf.Config.ProxyIP
	}
	s := &Server{
		PassWord:   conf.Config.PassWord,
		ServerName: conf.Config.Name,
		IPVersion:  "tcp4",
		IP:         "0.0.0.0",
		Port:       conf.Config.TcpPort,
		ProxyIP:    curProxyIP,
		DeployMode: conf.Config.DeployMode,
	}

	return s

}
func (s *Server) Start() {
	go func() {
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.Port))
		if err != nil {
			fmt.Println("ResolveTCPAddr err:", err)
		}
		listenner, err := net.ListenTCP("tcp4", addr)
		if err != nil {
			fmt.Println("ListenTCP err:", err)
		}
		ConnCurid := uint32(0)
		for {
			fmt.Println("listenner AcceptTCPing....")
			conn, err := listenner.AcceptTCP() //todo 连接校验
			if err != nil {
				fmt.Println("AcceptTCP err:", err)
			}
			dealConn := NewConnection(ConnCurid, conn)
			fmt.Println("New Connection:", dealConn.ConnId)
			ConnCurid++
			if ConnCurid >= math.MaxUint32 {
				ConnCurid = 0
			}
			go dealConn.Start()
		}
	}()

}

func (s *Server) Stop() { //todo

}

func (s *Server) Serve() {

	if s.DeployMode == "ProxyMode" {
		go s.registerToProxy()
	} else {
		go s.Start()
	}

	fmt.Println("---------------{" + s.ServerName + "}Start!---------------")
	select {}
}
func (s *Server) registerToProxy() { //todo 服务端向proxy注册时的校验
	conn, err := net.Dial("tcp4", s.ProxyIP)
	conn.Write([]byte(conf.Config.PassWord + "\r\n"))
	reader := bufio.NewReader(conn)
	reply, err := reader.ReadBytes('\n')
	if err != nil {
		panic("ReadBytes err")

	}
	if string(reply) == Common.RES_OK {
		//go gOCacheTemplate.SendHeartBeat()
		//go s.SendHeartbeatToProxy(conn.(*net.TCPConn))
		ConnCurid := uint32(0)
		ConnFromProxy := NewConnection(ConnCurid, conn.(*net.TCPConn))
		go ConnFromProxy.StartReaderAndWriter() //接下来就像正常连接一样
		log.Println("registerToProxy success!")
	} else {
		panic("Wrong PassWord")
		//return nil, errors.New("Wrong PassWord")
	}
}

func (s *Server) SendHeartbeatToProxy(conn *net.TCPConn) { //心跳检测要不要单独开一个连接
	//conn, err := net.Dial("tcp4", s.ProxyIP)
	//conn.Write([]byte(conf.Config.PassWord + "\r\n"))
	//reader := bufio.NewReader(conn)
	//reply, err := reader.ReadBytes('\n')
	//if err != nil {
	//	panic("ReadBytes err")
	//
	//}
	//if string(reply) == Common.CmdOK {
	for {
		sendData := Common.HeartBeat(strconv.FormatInt(time.Now().Unix(), 10), "HeartBeat From:"+conn.LocalAddr().String()) //心跳数据发什么 发送当前时间戳 val为空
		//同步阻塞!!!
		_, err := conn.Write(sendData) //每隔一秒发送
		if err != nil {
			log.Println("lost connect with proxy!", err)
		}
		time.Sleep(time.Second * 1)
	}

	//} else {
	//	panic("Wrong PassWord")
	//	//return nil, errors.New("Wrong PassWord")
	//}
}
