package CacheServer

import (
	"GOCache/Common"
	"GOCache/conf"
	"bufio"
	"errors"
	"fmt"
	"net"
	"time"
)

type IConnection interface {
	Start()
	Stop()
	GetTCPConn() *net.TCPConn
	GetConnId() uint32
	SendMsg(MsgId uint32, data []byte) error
	RemoteAddr() net.Addr

	//当我们在使用链接处理的时候，希望和链接绑定一些用户的数据，或者参数。那么我们现在可以把当前链接设定一些传递参数的接口或者方法。
	SetProperty(ket string, val interface{})
	GetProperty(ket string) (interface{}, error)
	RemoveProperty(ket string)
}
type Connection struct {
	ConnId        uint32
	Conn          *net.TCPConn
	MsgChan       chan []byte
	HeartBeatChan chan string
	isClosed      bool
	ExitChan      chan bool
}

func NewConnection(ConnId uint32, conn *net.TCPConn) *Connection {
	return &Connection{
		ConnId:        ConnId,
		Conn:          conn,
		isClosed:      false,
		ExitChan:      make(chan bool, 1),
		MsgChan:       make(chan []byte),
		HeartBeatChan: make(chan string, 6),
	}
}
func (c *Connection) Start() {
	//校验密码
	err := c.authPassWord()
	if err != nil {
		c.Conn.Write([]byte(Common.RES_WRONG_PASSWORD))
		c.Stop()
		return
	} else {
		c.Conn.Write([]byte(Common.RES_OK))
	}
	//开启读写协程
	//go c.HeartbeatDetector() //开启心跳检测
	c.StartReaderAndWriter()

	//这里可以加一个触发器（连接建立后）函数
}
func (c *Connection) StartReaderAndWriter() {
	go c.Reader()
	go c.Writer()
}
func (c *Connection) authPassWord() error {
	reader := bufio.NewReader(c.Conn)
	passWordBytes, err := reader.ReadBytes('\n')
	passWordBytes = passWordBytes[:len(passWordBytes)-2]
	if err != nil {
		return err
	}
	if string(passWordBytes) == conf.Config.PassWord {
		return nil
	} else {
		return errors.New("Wrong PassWord!!!")
	}
}

func (c *Connection) Reader() {
	defer c.Stop()
	for {
		bufOneByte := make([]byte, 2)
		unUsed := make([]byte, 2) //读两个换行符
		_, err := c.Conn.Read(bufOneByte[:1])
		if err != nil {
			return
		}
		switch bufOneByte[0] {
		case '*': //多行字符
			_, err := c.Conn.Read(bufOneByte[1:2])
			c.Conn.Read(unUsed)
			if err != nil {
				fmt.Println("Conn.Read err", err)
				return
			}
			cmd, key, val, err := Common.ReadMultiLineStringProtocol(c.Conn, int(bufOneByte[1]-'0')) //输入参数为几行
			if err != nil {
				fmt.Println("ReadMultiLineStringProtocol err", err)
				return
			}
			if cmd == Common.CmdHeartBeat { //心跳包转发给chan
				c.HeartBeatChan <- key
				c.HeartBeatChan <- val
			} else {
				go Handler(c, cmd, key, val)
			}

		default:
			fmt.Println("Don't Have Such Cmd")
		}
	}

}

func (c *Connection) Writer() {
	fmt.Println("connId=", c.ConnId, "Writer Goroutine is running")
	defer fmt.Println(c.ConnId, "Writer Goroutine is exited")
	for {
		select {
		case data := <-c.MsgChan:
			if _, err := c.Conn.Write(data); err != nil {
				fmt.Println("Write err", err)
				return
			}
		case <-c.ExitChan:
			return
		}
	}
}

func (c *Connection) HeartbeatDetector() {
	fmt.Println("connId=", c.ConnId, "HeartbeatDetector Goroutine is running")
	defer fmt.Println(c.ConnId, "HeartbeatDetector Goroutine is exited")
	idleDelay := time.NewTimer(time.Millisecond * 500)
	for {
		idleDelay.Reset(time.Millisecond * 500)
		select {
		case <-idleDelay.C: //超时5秒则执行关闭连接  应使用
			fmt.Println("HeartbeatDetector:", "连接超时未响应！")
			c.Stop()
			return
		case res := <-c.HeartBeatChan:
			fmt.Println("HeartbeatDetector:", res)
		case <-c.ExitChan:
			return
		}
	}
}

func (c *Connection) Stop() { //这里有并发冲突
	fmt.Println("connId:", c.ConnId, "stop")
	if c.isClosed == true {
		return
	} else {
		c.isClosed = true
		fmt.Println("connId:", c.ConnId, "stoping!!!!")
		c.Conn.Close()
		c.ExitChan <- true
		close(c.ExitChan)
		close(c.MsgChan)
		close(c.HeartBeatChan)

	}
}

func (c *Connection) GetTCPConn() *net.TCPConn {
	return c.Conn
}

func (c *Connection) GetConnId() uint32 {
	return c.ConnId
}

func (c *Connection) SendMsg(data []byte) error {
	if c.isClosed == true {
		return errors.New("Conn is closed!")
	}
	c.MsgChan <- data //发送给chan
	return nil
}

func (c *Connection) RemoteAddr() net.Addr {
	return c.Conn.RemoteAddr()
}

func (c *Connection) SetProperty(ket string, val interface{}) {

}
func (c *Connection) GetProperty(ket string) (interface{}, error) {
	return nil, nil
}
func (c *Connection) RemoveProperty(ket string) {

}
