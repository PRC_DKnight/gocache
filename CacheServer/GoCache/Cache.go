package GoCache

import (
	CacheAlgorithm2 "GOCache/CacheServer/GoCache/CacheAlgorithm"
	"sync"
)

//cache.go的实现非常简单，实例化 lru(或者其他算法)，封装 get 和 put 方法，并添加互斥锁 mu。
type Cache struct {
	lock       sync.Mutex                      //互斥锁
	CacheImpl  CacheAlgorithm2.ICacheAlgorithm //实现了ICacheAlgorithm的算法，现在只有LRU
	algorithm  string                          //使用的底层算法 例如"LRU"
	cacheBytes uint64                          //最大可存储的比特大小
}

func (c *Cache) newCache(algorithm string) CacheAlgorithm2.ICacheAlgorithm {
	switch c.algorithm {
	case "lru": //执行实例化  可添加其他算法
		return CacheAlgorithm2.NewLRUCache(c.cacheBytes)
	default:
		return CacheAlgorithm2.NewLRUCache(c.cacheBytes)
	}
}

func (c *Cache) put(key string, value ByteView) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if c.CacheImpl == nil { //延迟初始化
		c.CacheImpl = c.newCache(c.algorithm)
	}
	c.CacheImpl.Put(key, value)
}

func (c *Cache) get(key string) (value ByteView, ok bool) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if c.CacheImpl == nil { //延迟初始化
		c.CacheImpl = c.newCache(c.algorithm)
	}
	if v, ok := c.CacheImpl.Get(key); ok {
		return v.(ByteView), ok
	}

	return
}
