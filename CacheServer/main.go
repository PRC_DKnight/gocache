package main

import (
	"GOCache/CacheServer/GoCache/CacheServer"
)

func main() {
	s := CacheServer.NewServer()
	s.Serve()
}
