package GOCacheTemplate

import (
	"GOCache/Common"
	"bufio"
	"errors"
	"net"
	"strconv"
	"time"
)

type IGOCacheTemplate interface {
	Set()
	Get()
}
type GOCacheTemplate struct {
	Conn     net.Conn
	PassWord string
	TimeOut  time.Duration //客户端超时时间
}

func NewGOCacheTemplate(ip string, passWord string) (*GOCacheTemplate, error) {
	conn, err := net.Dial("tcp4", ip)
	conn.Write([]byte(passWord + "\r\n"))
	reader := bufio.NewReader(conn)
	reply, err := reader.ReadBytes('\n')
	if err != nil {
		//panic("ReadBytes err")
		return nil, err
	}
	gOCacheTemplate := &GOCacheTemplate{Conn: conn, PassWord: "8035aaaa"}
	if string(reply) == Common.RES_OK {
		//go gOCacheTemplate.SendHeartBeat()
		return gOCacheTemplate, nil
	} else {
		//panic("Wrong PassWord")
		return nil, errors.New("Wrong PassWord")
	}
}

func (t *GOCacheTemplate) SendHeartBeat() {
	for {
		sendData := Common.HeartBeat(strconv.FormatInt(time.Now().Unix(), 10), "HeartBeat From:"+t.Conn.LocalAddr().String()) //心跳数据发什么 发送当前时间戳 val为空
		//同步阻塞!!!
		t.Conn.Write(sendData) //每隔一秒发送
		time.Sleep(time.Second * 1)
	}

}

func (t *GOCacheTemplate) Set(key string, val string) (bool, error) {
	sendData := Common.SET(key, val)
	//同步阻塞!!!
	t.Conn.Write(sendData)
	reader := bufio.NewReader(t.Conn)
	bytesline, err := reader.ReadBytes('\n') //读一行的bytes
	if err != nil {
		return false, err
	}
	if string(bytesline) == Common.RES_OK {
		return true, nil
	} else {
		return false, nil
	}
}

//有err证明没找到
func (t *GOCacheTemplate) Get(key string) (string, error) {
	sendData := Common.GET(key)
	//同步阻塞!!!
	t.Conn.Write(sendData)
	reader := bufio.NewReader(t.Conn)
	bytesline, err := reader.ReadBytes('\n') //读一行的bytes
	if err != nil {
		return "", err
	}
	return string(bytesline[1:]), nil
}
