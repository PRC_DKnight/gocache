package GOCacheTemplate

import "net"

type GOCacheConn struct {
	Conn net.Conn
}

func NewGOCacheConn() *GOCacheConn {
	conn, err := net.Dial("tcp4", "0.0.0.0:9999")
	if err != nil {
		panic(1)
	}
	return &GOCacheConn{
		Conn: conn,
	}
}
