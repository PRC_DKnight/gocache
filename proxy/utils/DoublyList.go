package utils

type DoublyList struct {
	head *ListNode
	tail *ListNode
}
type IDoublyList interface {
	RemoveFirst() interface{}
	AddLast(val interface{})
}
type ListNode struct {
	val  interface{}
	next *ListNode
	pre  *ListNode
}

func NewListNode(val interface{}) *ListNode {
	return &ListNode{val: val, next: nil, pre: nil}
}
func NewDoublyList() *DoublyList {
	doublyList := &DoublyList{head: NewListNode(nil), tail: NewListNode(nil)}
	doublyList.head.next = doublyList.tail
	doublyList.tail.pre = doublyList.head
	return doublyList
}

func (d *DoublyList) RemoveFirst() interface{} {
	delNode := d.head.next
	nextNode := delNode.next
	d.head.next = nextNode
	nextNode.pre = d.head
	return delNode.val
}
func (d *DoublyList) AddLast(val interface{}) {
	newNode := &ListNode{val: val, next: nil, pre: nil}
	preNode := d.tail.pre
	preNode.next = newNode
	d.tail.pre = newNode
	newNode.pre = preNode
	newNode.next = d.tail
}
