package proxy

import (
	"GOCache/Common"
	"GOCache/conf"
	"GOCache/proxy/utils"
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"time"
)

type Node struct {
	conn          net.Conn
	HeartBeatChan chan string
	ExitChan      chan bool
	MsgChan       chan []byte
	id            int
	reader        *bufio.Reader
	MsgQueue      *utils.DoublyList
}

func NewNode(conn net.Conn, id int) *Node {
	return &Node{id: id,
		conn:          conn,
		reader:        bufio.NewReader(conn),
		HeartBeatChan: make(chan string),
		ExitChan:      make(chan bool),
		MsgChan:       make(chan []byte),
		MsgQueue:      utils.NewDoublyList(),
	}

}

func (n *Node) authPassWord() error {
	reader := bufio.NewReader(n.conn)
	passWordBytes, err := reader.ReadBytes('\n')
	passWordBytes = passWordBytes[:len(passWordBytes)-2]
	if err != nil {
		return err
	}
	if string(passWordBytes) == conf.Config.PassWord {
		n.conn.Write([]byte(Common.RES_OK))
		return nil
	} else {
		return errors.New("Wrong PassWord!!!")
	}
}

func (c *Node) readLine() ([]byte, error) {
	line, err := c.reader.ReadBytes('\n')
	if len(line) <= 2 {
		log.Println("readLine err len(line)<=2,May Node is Down")
		return nil, err
	}
	return line[:len(line)-2], err
}

func (n *Node) dealConn() { //todo 处理节点的连接 接收数据包 处理心跳包 如何原封不动转发消息？
	if n.authPassWord() != nil {
		return
	}
	//go n.HeartbeatDetector()
	go n.Writer()
	defer n.Stop()
	for {
		firstLine, err := n.readLine()
		if err != nil {
			log.Println("Read FirstLine err!May Node is Down")
			return
		}
		var cmd, key, val string
		switch firstLine[0] {
		case '-':
			n.sendBackToClient(append(append(firstLine, byte('\r'), byte('\n'))))
		case '+':
			n.sendBackToClient(append(append(firstLine, byte('\r'), byte('\n'))))
		case '*':
			dataLines, _ := strconv.Atoi(string(firstLine[1:])) //行数一个uint8就够了
			for i := 0; i < dataLines; i++ {
				line, err := n.readLine()
				if err != nil {
					log.Println("Read Lines err!")
					return
				}
				if line[0] != '$' {
					log.Println("Read Lines err!")
					return
				}

				lineLen, err := strconv.Atoi(string(line[1:])) //这里可能数很大
				if err != nil {
					log.Println("strconv.Atoi err!")
				}

				databuf := make([]byte, lineLen+2)
				io.ReadFull(n.reader, databuf)
				data := databuf[:len(databuf)-2]
				if i == 0 {
					cmd = string(data)
				}
				if i == 1 {
					key = string(data)
				}
				if i == 2 {
					val = string(data)
				}
			}
			var sendData []byte
			switch cmd {
			case Common.CmdGet:
				sendData = Common.GET(key)
			case Common.CmdSet:
				sendData = Common.SET(key, val)
			case Common.CmdOK:
				sendData = []byte(Common.RES_OK)
				if cmd == Common.CmdHeartBeat {
					n.HeartBeatChan <- cmd + " " + key + " " + val
				} else {
					n.sendBackToClient(sendData) //todo 如何找到是谁发给他的
				}

			default:
				fmt.Println("Don't Have Such Cmd")
			}
		}
	}
}

func (c *Node) sendBackToClient(sendData []byte) {
	clientConn := c.MsgQueue.RemoveFirst().(*ClientConn)
	clientConn.SendMsg(sendData)
}

func (c *Node) Writer() {
	fmt.Println("connId=", c.id, "Writer Goroutine is running")
	defer fmt.Println(c.id, "Writer Goroutine is exited")
	for {
		select {
		case data := <-c.MsgChan:
			if _, err := c.conn.Write(data); err != nil {
				fmt.Println("Write err", err)
				return
			}
		case <-c.ExitChan:
			return
		}
	}
}

func (n *Node) HeartbeatDetector() {
	fmt.Println("connId=", n.id, "HeartbeatDetector Goroutine is running")
	defer fmt.Println(n.id, "HeartbeatDetector Goroutine is exited")
	idleDelay := time.NewTimer(time.Second * 5)
	for {
		idleDelay.Reset(time.Second * 5)
		select {
		case <-idleDelay.C: //超时5秒则执行关闭连接  应使用
			fmt.Println("HeartbeatDetector:", "连接超时未响应！")
			n.Stop()
			return
		case <-n.HeartBeatChan:
			//fmt.Println("HeartbeatDetector:", res)
		case <-n.ExitChan:
			return
		}
	}
}

func (n *Node) Stop() {

}
