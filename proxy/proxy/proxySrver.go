package proxy

import (
	"fmt"
	"hash/crc32"
	"log"
	"math"
	"net"
	"sort"
	"strconv"
)

type IServer interface {
	Start()
	Stop()
	Serve()
}

var DefaultProxyServer *ProxyServer

func init() {
	DefaultProxyServer = NewProxyServer()
}

type Hash func(data []byte) uint32

type ProxyServer struct {
	//ServerID   uint32
	ServerName       string
	IPVersion        string
	IP               string
	TcpPortForNode   int    //监听节点的端口
	TcpPortForClient int    //监听客户端的端口
	PassWord         string //加入校验密码，在连接建立时交换，若不正确则关闭连接
	NodeMap          map[int]*Node
	ClientConnMap    map[uint32]*ClientConn
	hash             Hash
	replicas         int   //虚拟节点倍数
	hashLoop         []int // Sorted
}

func NewProxyServer() *ProxyServer {
	s := &ProxyServer{
		PassWord:         ProxyDefaultConfig.PassWord,
		ServerName:       ProxyDefaultConfig.Name,
		IPVersion:        "tcp4",
		IP:               "0.0.0.0",
		hash:             crc32.ChecksumIEEE,
		replicas:         3,
		NodeMap:          make(map[int]*Node),
		ClientConnMap:    make(map[uint32]*ClientConn),
		TcpPortForNode:   ProxyDefaultConfig.TcpPortForNode,   //监听节点的端口
		TcpPortForClient: ProxyDefaultConfig.TcpPortForClient, //监听客户端的端口
	}

	return s

}
func (s *ProxyServer) DealNodeConn() {
	go func() {
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.TcpPortForNode))
		if err != nil {
			fmt.Println("ResolveTCPAddr err:", err)
		}
		listenner, err := net.ListenTCP("tcp4", addr)
		if err != nil {
			fmt.Println("ListenTCP err:", err)
		}
		NodeId := int(0)
		for {
			fmt.Println("listenner Accept From Node....")
			conn, err := listenner.AcceptTCP() //todo 连接校验
			if err != nil {
				fmt.Println("AcceptTCP err:", err)
			}
			curNode := NewNode(conn, NodeId)
			fmt.Println("New Connection From Node:", curNode.id)
			NodeId++
			if NodeId >= math.MaxInt {
				NodeId = 0
			}
			s.Add(curNode)
			s.NodeMap[curNode.id] = curNode
			go curNode.dealConn()
		}
	}()
}

func (s *ProxyServer) Start() {
	go func() {
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.TcpPortForClient))
		if err != nil {
			fmt.Println("ResolveTCPAddr err:", err)
		}
		listenner, err := net.ListenTCP("tcp4", addr)
		if err != nil {
			fmt.Println("ListenTCP err:", err)
		}
		ConnCurid := uint32(0)
		for {
			fmt.Println("listenner Accept TCP From Client....")
			conn, err := listenner.AcceptTCP() //todo 连接校验
			if err != nil {
				fmt.Println("AcceptTCP err:", err)
			}
			dealConn := NewConnection(ConnCurid, conn)
			err = dealConn.authPassWord()
			if err != nil {
				log.Println(err)
				log.Println("New Connection But Refuse Because Of Wrong Password:", dealConn.Conn.RemoteAddr())
			}
			fmt.Println("New Connection From Client :", dealConn.ConnId)
			s.ClientConnMap[ConnCurid] = dealConn
			ConnCurid++
			if ConnCurid >= math.MaxUint32 {
				ConnCurid = 0
			}

			go dealConn.ReaderAndSendToNode()
		}
	}()
}

func (s *ProxyServer) Stop() { //todo

}

func (s *ProxyServer) Serve() {
	go s.DealNodeConn()
	go s.Start()
	fmt.Println("---------------{" + s.ServerName + "}Start!---------------")
	select {}
}

func (m *ProxyServer) Add(Nodes ...*Node) {
	for _, Node := range Nodes {
		for i := 0; i < m.replicas; i++ { //对每个真实节点映射为replicas个虚拟节点
			hash := int(m.hash([]byte(strconv.Itoa(i) + string(Node.id)))) // Itoa is equivalent to FormatInt(int64(i), 10).
			m.hashLoop = append(m.hashLoop, hash)
			m.NodeMap[hash] = Node
		}
	}
	sort.Ints(m.hashLoop)
}

// Get gets the closest item in the hash to the provided key.
func (m *ProxyServer) Get(key string) *Node {
	if len(m.hashLoop) == 0 {
		return nil
	}

	hash := int(m.hash([]byte(key)))

	//index := sort.Search(n int,f func(i int) bool) int
	//该函数使用二分查找的方法，会从[0, n)中取出一个值index，index为[0, n)中最小的使函数f(index)为True的值，并且f(index+1)也为True。
	//如果无法找到该index值，则该方法为返回n。

	idx := sort.Search(len(m.hashLoop), func(i int) bool { //要求哈希环必须递增
		return m.hashLoop[i] >= hash
	})

	return m.NodeMap[m.hashLoop[idx%len(m.hashLoop)]]
}
