package proxy

import (
	"GOCache/Common"
	"GOCache/conf"
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
)

type IConnection interface {
	Start()
	Stop()
	GetTCPConn() *net.TCPConn
	GetConnId() uint32
	SendMsg(MsgId uint32, data []byte) error
	RemoteAddr() net.Addr

	//当我们在使用链接处理的时候，希望和链接绑定一些用户的数据，或者参数。那么我们现在可以把当前链接设定一些传递参数的接口或者方法。
	SetProperty(ket string, val interface{})
	GetProperty(ket string) (interface{}, error)
	RemoveProperty(ket string)
}
type ClientConn struct {
	ConnId        uint32
	Conn          *net.TCPConn
	MsgChan       chan []byte
	HeartBeatChan chan string
	isClosed      bool
	ExitChan      chan bool
	reader        *bufio.Reader
	writer        *bufio.Writer
}

func NewConnection(ConnId uint32, conn *net.TCPConn) *ClientConn {
	return &ClientConn{
		ConnId:        ConnId,
		Conn:          conn,
		isClosed:      false,
		ExitChan:      make(chan bool, 1),
		MsgChan:       make(chan []byte),
		HeartBeatChan: make(chan string, 6),
		reader:        bufio.NewReader(conn),
		writer:        bufio.NewWriter(conn),
	}
}

func (c *ClientConn) Start() {

}

func (c *ClientConn) authPassWord() error {
	reader := bufio.NewReader(c.Conn)
	passWordBytes, err := reader.ReadBytes('\n')
	passWordBytes = passWordBytes[:len(passWordBytes)-2]
	if err != nil {
		return err
	}
	if string(passWordBytes) == conf.Config.PassWord {
		c.Conn.Write([]byte(Common.RES_OK))
		return nil
	} else {
		return errors.New("Wrong PassWord!!!")
	}
}

func (c *ClientConn) readLine() ([]byte, error) {
	line, err := c.reader.ReadBytes('\n')
	if len(line) <= 2 {
		log.Println("readLine err len(line)<=2")
		c.Stop()
		return nil, err
	}
	return line[:len(line)-2], err
}

func (c *ClientConn) ReaderAndSendToNode() { //todo 要读包，但是如何保留原始数据包并转发
	go c.Writer()
	defer c.Stop()
	for {
		if c.isClosed {
			return
		}
		firstLine, err := c.readLine()
		if err != nil {
			log.Println("Read FirstLine err!")
			return
		}
		var cmd, key, val string
		switch firstLine[0] {
		case '*':
			dataLines, _ := strconv.Atoi(string(firstLine[1:])) //行数一个uint8就够了
			for i := 0; i < dataLines; i++ {
				line, err := c.readLine()
				if err != nil {
					log.Println("Read Lines err!")
					return
				}
				if line[0] != '$' {
					log.Println("Read Lines err!")
					return
				}

				lineLen, err := strconv.Atoi(string(line[1:])) //这里可能数很大
				if err != nil {
					log.Println("strconv.Atoi err!")
				}

				databuf := make([]byte, lineLen+2)
				io.ReadFull(c.reader, databuf)
				data := databuf[:len(databuf)-2]
				if i == 0 {
					cmd = string(data)
				}
				if i == 1 {
					key = string(data)
				}
				if i == 2 {
					val = string(data)
				}
			}
			err := c.sendToNode(cmd, key, val)
			if err != nil {
				continue
			}
			//cmd, key, val, err := Common.ReadMultiLineStringProtocol(c.Conn, int(bufOneByte[1]-'0')) //输入参数为几行
			//if err != nil {
			//	fmt.Println("ReadMultiLineStringProtocol err", err)
			//	return
			//}
			//if cmd == Common.CmdHeartBeat { //心跳包转发给chan
			//	c.HeartBeatChan <- key
			//	c.HeartBeatChan <- val
			//} else { //todo 这里选择要选择的节点
			//
			//}

		default:
			fmt.Println("Don't Have Such Cmd")
		}
	}

}

func (c *ClientConn) sendToNode(cmd string, key string, val string) error {
	Node := DefaultProxyServer.Get(key)
	if Node == nil {
		log.Println("There is No Any registered Node")
		c.MsgChan <- []byte(Common.RES_ERR_NOT_FOUND)
		c.Stop()
		return errors.New("There is No Any registered Node")
	}
	var sendData []byte
	switch cmd {
	case Common.CmdGet:
		sendData = Common.GET(key)
	case Common.CmdSet:
		sendData = Common.SET(key, val)
	default:
		log.Println("sendToNode err:don't have such cmd")
	}
	if sendData != nil {
		Node.MsgQueue.AddLast(c) //加入Node的请求排队列，返回时对应着队头的请求，前进先出，tcp保证传输可靠，proxy只提供超时检测
		Node.conn.Write(sendData)
		//err:=c.readAndSendBackByLine()
		//if err!=nil{
		//	log.Println("readAndSendBackByLine err")
		//	return err
		//}

	}
	return nil
}

func (c *ClientConn) Writer() {
	fmt.Println("connId=", c.ConnId, "Writer Goroutine is running")
	defer fmt.Println(c.ConnId, "Writer Goroutine is exited")
	for {
		select {
		case data := <-c.MsgChan:
			if _, err := c.Conn.Write(data); err != nil {
				fmt.Println("Write err", err)
				return
			}
		case <-c.ExitChan:
			return
		}
	}
}

func (c *ClientConn) Stop() { //这里有并发冲突
	fmt.Println("connId:", c.ConnId, "stop")
	if c.isClosed == true {
		return
	} else {
		c.isClosed = true
		fmt.Println("connId:", c.ConnId, "stoping!!!!")
		c.Conn.Close()
		c.ExitChan <- true
		close(c.ExitChan)
		close(c.MsgChan)
		close(c.HeartBeatChan)

	}
}

func (c *ClientConn) GetTCPConn() *net.TCPConn {
	return c.Conn
}

func (c *ClientConn) GetConnId() uint32 {
	return c.ConnId
}

func (c *ClientConn) SendMsg(data []byte) error {
	if c.isClosed == true {
		return errors.New("Conn is closed!")
	}
	c.MsgChan <- data //发送给chan
	return nil
}

func (c *ClientConn) RemoteAddr() net.Addr {
	return c.Conn.RemoteAddr()
}

func (c *ClientConn) SetProperty(ket string, val interface{}) {

}
func (c *ClientConn) GetProperty(ket string) (interface{}, error) {
	return nil, nil
}
func (c *ClientConn) RemoveProperty(ket string) {

}
