package proxy

import (
	"encoding/json"
	"io/ioutil"
)

//实例化一个全部配置类，这个配置类会读取外部json文件
var ProxyDefaultConfig *ProxyConfig

type ProxyConfig struct {
	MaxTransBytes    uint64 //最大数据包比特数
	Host             string //绑定监听ip
	TcpPortForNode   int    //监听节点的端口
	TcpPortForClient int    //监听客户端的端口
	Name             string //该服务器实例的名字
	Version          string //版本号
	PassWord         string //全局密码
	replicas         int
	Hash             Hash
	//MaxPackageSize   uint32
	//WorkerPoolSize   uint32
	//MaxWorkerTaskLen uint32 //能排队的最大值
}

func (g *ProxyConfig) Load() {

	data, err := ioutil.ReadFile("./GOCacheProxy.json")

	if err != nil { //不存在文件
		return
	}
	err = json.Unmarshal(data, &ProxyDefaultConfig)
	if err != nil {
		panic(err)
	}
}

func init() {
	ProxyDefaultConfig = &ProxyConfig{
		PassWord:         "8035aaaa",
		Host:             "0.0.0.0",
		Name:             "GOCacheProxy",
		Version:          "v0.3",
		TcpPortForNode:   9998,
		TcpPortForClient: 9999,
		MaxTransBytes:    9192,
		//MaxPackageSize:   4096,
		//WorkerPoolSize:   10,
		//MaxWorkerTaskLen: 1024,
	}
	ProxyDefaultConfig.Load()

}
