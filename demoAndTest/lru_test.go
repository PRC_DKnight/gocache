package main

import (
	CacheAlgorithm2 "GOCache/CacheServer/GoCache/CacheAlgorithm"
	"fmt"
	"testing"
)

func TestLRU(t *testing.T) {
	fmt.Println(177 + 100 + 79 + 145 + 50 + 54 + 391 + 161 + 198 + 152 + 46 + 500)
	cache := CacheAlgorithm2.NewLRUCache(5)
	cache.Put("1", CacheAlgorithm2.String("a"))
	cache.Put("2", CacheAlgorithm2.String("b"))
	cache.Put("3", CacheAlgorithm2.String("c"))
	cache.Put("4", CacheAlgorithm2.String("d"))
	cache.Put("5", CacheAlgorithm2.String("e"))
	fmt.Println(cache.Get("4"))
	fmt.Println(cache.Get("2"))
	fmt.Println(cache.Get("3"))
	fmt.Println(cache.Get("4"))
}
