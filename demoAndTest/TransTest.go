package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"strconv"
)

//为了搞明白 大小端传输
func main() {
	a := int(5)
	b := uint32(5)

	aString := strconv.Itoa(a)
	dataBuff := bytes.NewBuffer([]byte{})
	binary.Write(dataBuff, binary.LittleEndian, []byte(aString))
	res := dataBuff.Bytes()
	fmt.Println("将int转化为string后用小端编码", res)

	fmt.Println("小端序测试")
	dataBuff1 := make([]byte, 4)
	binary.LittleEndian.PutUint32(dataBuff1, b)
	fmt.Println("小端序转码后：", dataBuff1)
	dataBuff2 := make([]byte, 4)
	binary.BigEndian.PutUint32(dataBuff2, b)
	fmt.Println("大端序转码后：", dataBuff2)
	fmt.Println("小端序解码后：", binary.LittleEndian.Uint32(dataBuff1))
	fmt.Println("小端序用大端序解码后：", binary.BigEndian.Uint32(dataBuff1))
	//将int转化为string后用小端编码 [49 50 51 52 53 54 55 56 57]
	//小端序测试  dataBuff的长度必须在4字节以上（uint32）
	//小端序转码后： [21 205 91 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//大端序转码后： [7 91 205 21 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//小端序解码后： 123456789
	//小端序用大端序解码后： 365779719
	//将数字单独编码明显减少使用空间
}
