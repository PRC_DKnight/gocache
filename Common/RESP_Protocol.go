package Common

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"io"
	"net"
	"strconv"
)

const (
	CmdPing                = "PING"
	CmdSet                 = "SET"
	CmdGet                 = "GET"
	CmdOK                  = "+OK"
	RES_OK                 = "+OK\r\n"
	CmdHeartBeat           = "HEARTBEAT"
	RES_WRONG_PASSWORD     = "WRONG_PASSWORD\r\n"
	RES_ERR_NOT_FOUND      = "-ERR NOT_FOUND\r\n"
	RES_ERR_Invalid_Synatx = "-ERR Invalid Synatx\r\n"
)

//*3 代表接下来有3行数据 例如：set key val三行 $3代表这行有3字节长度 $后跟的数组采用小端编码

func SET(key string, val string) []byte {
	data := "*3\r\n"
	data += "$3\r\n"
	data += "SET\r\n"
	data += "$" + strconv.Itoa(len(key)) + "\r\n"
	data += key + "\r\n"
	data += "$" + strconv.Itoa(len(val)) + "\r\n"
	data += val + "\r\n"
	dataBuff := bytes.NewBuffer([]byte{})
	binary.Write(dataBuff, binary.BigEndian, []byte(data))
	return dataBuff.Bytes()
}
func HeartBeat(key string, val string) []byte {
	data := "*3\r\n"
	data += "$9\r\n"
	data += CmdHeartBeat + "\r\n"
	data += "$" + strconv.Itoa(len(key)) + "\r\n"
	data += key + "\r\n"
	data += "$" + strconv.Itoa(len(val)) + "\r\n"
	data += val + "\r\n"
	dataBuff := bytes.NewBuffer([]byte{})
	binary.Write(dataBuff, binary.BigEndian, []byte(data))
	return dataBuff.Bytes()
}

func GET(key string) []byte {
	data := "*2\r\n"
	data += "$3\r\n"
	data += "GET\r\n"
	data += "$" + strconv.Itoa(len(key)) + "\r\n"
	data += key + "\r\n"
	dataBuff := bytes.NewBuffer([]byte{})
	binary.Write(dataBuff, binary.BigEndian, []byte(data))
	return dataBuff.Bytes()
}

type redisInputStream struct {
	*bufio.Reader
	buf   []byte
	count int
	limit int
	c     *net.Conn
}

func ReadStringProtocol(c net.Conn) (string, error) { //处理简单字符串 第一个符号为+
	reader := bufio.NewReader(c)
	for {
		// 读取一行数据，交给后台处理
		bytesline, err := reader.ReadBytes('\n') //读一行的bytes
		if err != nil {
			return "", err
		}
		return string(bytesline), nil
	}
}

func readLine(c net.Conn) ([]byte, error) {
	reader := bufio.NewReader(c)
	line, err := reader.ReadBytes('\n')
	return line[:len(line)-2], err
}

func ReadMultiLineStringProtocol(c net.Conn, i int) (string, string, string, error) {
	reader := bufio.NewReader(c)
	var cmd, key, val string
	for k := 0; k < i; k++ {
		nextBytesCount, err := reader.ReadBytes('\n') //读一行的bytes  $n
		//fmt.Println(string(nextBytesCount))
		if err != nil {
			return "", "", "", err
		}

		numBytes := nextBytesCount[1 : len(nextBytesCount)-2] // [0 0 0 0 0 0 1 255]
		n, _ := strconv.Atoi(string(numBytes))
		msg := make([]byte, n+2) // 正文长度nextBytesCount + 换行符长度2
		_, err = io.ReadFull(reader, msg)
		if err != nil {
			return "", "", "", err
		}
		if k == 0 {
			cmd = string(msg[:len(msg)-2]) //去掉换行符
		} else if k == 1 {
			key = string(msg[:len(msg)-2])
		} else {
			val = string(msg[:len(msg)-2])
		}
	}
	return cmd, key, val, nil
}
