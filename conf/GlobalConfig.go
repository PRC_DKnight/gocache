package conf

import (
	"encoding/json"
	"io/ioutil"
)

//实例化一个全部配置类，这个配置类会读取外部json文件
var Config *GlobalConfig

const (
	DeployWithProxy  = "ProxyMode"
	DeployWithSingle = "SingletonMode"
)

type GlobalConfig struct {
	MaxBytes       uint64 //最大存储比特数
	MaxTransBytes  uint64 //最大数据包比特数
	CacheAlgorithm string //使用的淘汰算法
	Host           string //绑定监听ip
	TcpPort        int    //绑定监听端口
	Name           string //该服务器实例的名字
	Version        string //版本号
	MaxConn        int    //最大连接数
	PassWord       string //密码 一个服务器对应一个密码
	DeployMode     string //部署模式 单机 代理集群
	ProxyIP        string
	//MaxPackageSize   uint32
	//WorkerPoolSize   uint32
	//MaxWorkerTaskLen uint32 //能排队的最大值
}

func (g *GlobalConfig) Load() {

	data, err := ioutil.ReadFile("./GOCacheProxy.json")

	if err != nil { //不存在文件
		return
	}
	err = json.Unmarshal(data, &Config)
	if err != nil {
		panic(err)
	}
}

func init() {
	Config = &GlobalConfig{
		PassWord:       "8035aaaa",
		MaxBytes:       2147483648,
		Host:           "0.0.0.0",
		TcpPort:        9999,
		Name:           "GOCacheServer",
		Version:        "v0.3",
		MaxConn:        10,
		CacheAlgorithm: "lru",
		ProxyIP:        "0.0.0.0:9998",
		DeployMode:     DeployWithProxy,
		//MaxPackageSize:   4096,
		//WorkerPoolSize:   10,
		//MaxWorkerTaskLen: 1024,
	}
	Config.Load()

}
